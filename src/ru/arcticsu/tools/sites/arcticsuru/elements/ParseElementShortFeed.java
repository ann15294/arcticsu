package ru.arcticsu.tools.sites.arcticsuru.elements;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.util.Log;
import ru.arcticsu.tools.sites.Parse;
import ru.arcticsu.tools.sites.arcticsuru.ConstantsArctisuru;
import ru.arcticsu.tools.sites.arcticsuru.ParseArcticsuDetailedFeed;

/**
 * Парсинг новостей.
 * @author Danilov E.Y.
 *
 */
public class ParseElementShortFeed extends ParseElement
{
	private String link;
	private String title;
	private String shortContent;
	private String urlImage;
	
	/**
	 * {@linkplain ParseElementShortFeed}
	 * @param element
	 */
    public ParseElementShortFeed(Element element)
    {
	    super(element);
	    
	    this.title = "";
	    
	    this.parsing();
    }

    public void parsing()
    {
		Element titleFeed = this.element.getElementsByClass(ConstantsArctisuru.FEED_CLASS_TITLE).first();
		this.title = titleFeed.text();

		this.link = titleFeed.child(0).attr("href");
		
		Element content = this.element.getElementsByClass(ConstantsArctisuru.FEED_CLASS_CONTENT).first();
		Element image =  null;
		Element shortFeedTextElement = null;
		
		for(Element p : content.getElementsByTag("p"))
		{
			if(p.hasText())
				shortFeedTextElement = p;
			else
				image = p;
		}
		
		this.shortContent = shortFeedTextElement.text();
		
    }

	public String getTitle()
	{
		return this.title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getShortContent()
	{
		return this.shortContent;
	}

	public void setShortContent(String shortContent)
	{
		this.shortContent = shortContent;
	}

	public String getUrlImage()
	{
		return this.urlImage;
	}

	public void setUrlImage(String urlImage)
	{
		this.urlImage = urlImage;
	}

	public String getLink()
	{
		return this.link;
	}

	public void setLink(String link)
	{
		this.link = link;
	}

}
