package ru.arcticsu.tools.sites.arcticsuru.elements;

import org.jsoup.nodes.Element;

/**
 * Парсинг элементов сайта.
 * @author Danilov E.Y.
 *
 */
public abstract class ParseElement implements ParseElementInt
{
	protected Element element;
	
	public ParseElement(Element element)
	{
		this.element = element;
	}
}
