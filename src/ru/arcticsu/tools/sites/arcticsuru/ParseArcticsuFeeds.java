package ru.arcticsu.tools.sites.arcticsuru;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import ru.arcticsu.tools.sites.Parse;
import ru.arcticsu.tools.sites.arcticsuru.elements.ParseElementShortFeed;
import android.util.Log;

/**
 * Парсинг сайта Arcticsu.ru.
 * @author Danilov E.Y.
 *
 */
public class ParseArcticsuFeeds extends Parse
{
	
	private ParseElementShortFeed[] arrayFeed;
	
	/**
	 * {@linkplain ParseArcticsuFeeds}
	 */
    public ParseArcticsuFeeds()
    {
	    super(ConstantsArctisuru.URL_ARCTICSU_RU_FEEDS);
	    if(this.parseDoc!= null)
	    	this.parsing();
    }
    
    public void parsing()
    {
    	Element elemViewContent = this.parseDoc.getElementsByClass(ConstantsArctisuru.FEED_CLASS_VIEW_CONTENT).first();
    	
    	Element elemFeedItemList = elemViewContent.getElementsByClass(ConstantsArctisuru.FEED_CLASS_ITEM_LIST).first();
    	
		Elements elemFeedsList = elemFeedItemList.getElementsByClass(ConstantsArctisuru.FEED_CLASS_VIEW_ROW);
		
		int countFeeds = elemFeedsList.size();
		
		Log.i("Array Size", String.valueOf(elemFeedsList.size()));
		
		this.arrayFeed = new ParseElementShortFeed[countFeeds];
    	
    	for(int i = 0; i<countFeeds; i++)
    	{
    		this.arrayFeed[i] = new ParseElementShortFeed(elemFeedsList.get(i));
    	}

    }

	public ParseElementShortFeed[] getArrayFeed()
	{
		return this.arrayFeed;
	}

}
