package ru.arcticsu.tools.sites.arcticsuru;

import java.util.ArrayList;

import org.jsoup.nodes.Element;

import android.util.Log;
import ru.arcticsu.tools.sites.Parse;

/**
 * @author Danilov
 *
 */
public class ParseArcticsuDetailedFeed extends Parse
{
	private ArrayList<String> urlImages;
	private String title = "title";
	private String content = "content";

	/**
	 * @param URL
	 */
    public ParseArcticsuDetailedFeed(String URL)
    {
	    super(URL);

	    if(this.parseDoc != null)
	    	this.parsing();
    }

    private void parsing()
    {
    	
    	Element content = this.parseDoc.getElementById("content-inner");
    	
    	Element titleFeed = content.child(0);
		this.title = titleFeed.text();
		
		Element content_content = content.getElementsByClass("content").first();
		
		/*Element image =  null;
		Element contentElement = null;
		
		for(Element p : content.getElementsByTag("p"))
		{
			if(p.hasText())
				contentElement = p;
			else
				image = p;
		}*/
		
		this.content = content_content.text();
    }

	/**
	 * @return the urlImages
	 */
	public ArrayList<String> getUrlImages()
	{
		return urlImages;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @return the content
	 */
	public String getContent()
	{
		return content;
	}
}
