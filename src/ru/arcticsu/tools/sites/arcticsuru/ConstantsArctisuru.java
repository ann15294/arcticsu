package ru.arcticsu.tools.sites.arcticsuru;

/**
 * @author Danilov E.Y.
 *
 */
public interface ConstantsArctisuru
{
	public static final String URL_ARCTICSU_RU = "http://www.arcticsu.ru";
	public static final String URL_ARCTICSU_RU_FEEDS = "http://www.arcticsu.ru/content/novosti";
	public static final String URL_ARCTICSU_RU_CONTACTS_IPM = "http://www.arcticsu.ru/content/o-fakultete";
	public static final String URL_ARCTICSU_RU_CONTACTS_ECO = "http://www.arcticsu.ru/content/o-fakultete-0";
	//=======================================
	public static final String FEED_CLASS_ITEM_LIST = "item-list";
	public static final String FEED_CLASS_VIEW_CONTENT = "view-content";
	public static final String FEED_CLASS_VIEW_ROW = "views-row";
	
	//=============Новость===================
	public static final String FEED_CLASS_CONTENT = "content";
	public static final String FEED_CLASS_TITLE = "title";
	public static final String FEED_CLASS_LINKS = "links";
}
