package ru.arcticsu.tools.sites;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import android.util.Log;

/**
 * Парсинг веб страници.
 * @author Danilov E.Y.
 *
 */
public abstract class Parse implements ParseInt
{
	protected String URL;
	protected Document parseDoc;
	protected String title;
	
	/**
	 * 
	 * {@linkplain Parse}
	 * @param URL - адресс страници.
	 */
	public Parse(String URL)
	{
		this.URL = URL;
		
		if(Parse.isURL(URL))
		{
			try
            {
	            this.parseDoc = Jsoup.connect(URL).data("query", "Java")
	            		  .userAgent("Mozilla")
	            		  .cookie("auth", "token")
	            		  .timeout(3000)
	            		  .post();
	            
	            this.title = this.parseDoc.title();
            }
            catch(IOException e)
            {
            	Log.e("ParseDoc", e.toString());
            	this.parseDoc = null;
            }
		}
	}
	
    /**
     * Проверка на правильность url-ссылки.
     * @param url - Ссылка.
     * @return - Возвращает true если это ссылка.
     */
    public static boolean isURL(String url)
    {
    	boolean urlCheck = false;
    	try
		{
			new java.net.URL(url);
			urlCheck = true;
		} 
    	catch (MalformedURLException e)
		{
    		urlCheck = false;
		}

    	return urlCheck;
    }
	
	public String getURL()
	{
	    return this.URL;
	}
	
	public void setURL(String URL)
	{
	    this.URL = URL;
	}

	public Document getParseDoc()
	{
		return this.parseDoc;
	}

	public void setParseDoc(Document parseDoc)
	{
		this.parseDoc = parseDoc;
	}
}
