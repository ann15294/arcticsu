/**
 * 
 */
package ru.arcticsu.activities;

import ru.arcticsu.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * @author Danilov
 *
 */
public class MoodleActivity extends RunnableActivity
{
	private WebView mWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview_activity);

	    
	    this.mWebView = (WebView) findViewById(R.id.webviewmoodle);
	    
	    WebSettings settings = this.mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDefaultZoom(ZoomDensity.FAR);
        //settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        this.mWebView.setInitialScale(0);
        String newUA= "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0";
        mWebView.getSettings().setUserAgentString(newUA);
        this.mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
	    
		// указываем страницу загрузки
	    this.mWebView.loadUrl("http://moodle.arcticsu.ru/"); 
	    
	    this.mWebView.setWebViewClient(new WebViewClient ()
	    {

	    	@Override
	    	public void onPageFinished(WebView view, String url)
	    	{
	    	    super.onPageFinished(view, url);
	    	    MoodleActivity.this.setTitle(view.getTitle());
	    	}
	    	@Override
	    	public boolean shouldOverrideUrlLoading(WebView view, String url)
	    	{
	            view.loadUrl(url);
	            return true;
	    	}
	    });
	    
	}
	

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
	    return this.mWebView.onTouchEvent(event);
	}
	
	@Override
	public void onBackPressed()
	{
	    if(this.mWebView.canGoBack()) {
	    	this.mWebView.goBack();
	    } 
	    else 
	    {
	    	RunnableActivity.position = 0;
	    	startActivity(new Intent(this, FeedsAcrivity.class));
	    }
	}
}
