/**
 * 
 */
package ru.arcticsu.activities;

import ru.arcticsu.R;
import ru.arcticsu.tools.Settings;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

/**
 * @author Danilov
 *
 */
public class SettingActivity extends Activity
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.setting_artivity);
	    
	    LinearLayout settingContent = (LinearLayout) findViewById(R.id.liner_layout_setting);
	    settingContent.setPadding(10, 10, 10, 10);
	    
	    LinearLayout loadPictureSettingContent = new LinearLayout(settingContent.getContext());
	    loadPictureSettingContent.setOrientation(LinearLayout.HORIZONTAL);
	    
	    TextView textEnablePic = new TextView(loadPictureSettingContent.getContext());
	    textEnablePic.setText("Загружать изображения");
	    textEnablePic.setPadding(0,0, 10, 0);
	    
	    Switch loadPic = new Switch(loadPictureSettingContent.getContext());
	    loadPic.setChecked(Settings.isLoadPic);
	    loadPic.setOnCheckedChangeListener(new OnCheckedChangeListener()
		{
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
			{
				Settings.isLoadPic = isChecked;				
			}
		});
	    
	    loadPictureSettingContent.addView(textEnablePic);
	    loadPictureSettingContent.addView(loadPic);
	    
	    settingContent.addView(loadPictureSettingContent);
	    
	    
	    
	    
	}
}
