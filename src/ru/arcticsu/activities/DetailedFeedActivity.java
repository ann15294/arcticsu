package ru.arcticsu.activities;

import ru.arcticsu.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author Danilov
 *
 */
public class DetailedFeedActivity extends Activity
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
	    
		setContentView(R.layout.detailed_feed);
		
		LinearLayout layout = (LinearLayout) findViewById(R.id.liner_detal_feed);
		
		String title = getIntent().getExtras().getString("title");
		String content = getIntent().getExtras().getString("content");
		
		TextView titleText = new TextView(this);
		titleText.setText(title);
		
		TextView contentText = new TextView(this);
		contentText.setText(content);
		
		
		
		layout.addView(titleText);
		layout.addView(contentText);
		
	}
}
