package ru.arcticsu.activities.gui;

import java.security.spec.MGF1ParameterSpec;

import ru.arcticsu.R.drawable;
import ru.arcticsu.activities.DetailedFeedActivity;
import ru.arcticsu.tools.sites.arcticsuru.ConstantsArctisuru;
import ru.arcticsu.tools.sites.arcticsuru.ParseArcticsuDetailedFeed;
import ru.arcticsu.tools.sites.arcticsuru.elements.ParseElementShortFeed;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * @author Danilov E.Y.
 *
 */
public class FeedItemContent extends GuiElement
{
	private LinearLayout content;
	private Context context;
	private ParseElementShortFeed parseElement;
	 /**
     * {@linkplain FeedItemContent}
     */
    public FeedItemContent(Context context, ParseElementShortFeed parseElement)
    {
    	this.context = context;
	    this.parseElement = parseElement;
	    this.init();
    }
    
    private void init()
    {
    	GradientDrawable drawable = new GradientDrawable();
    	drawable.setShape(GradientDrawable.RECTANGLE);
    	drawable.setStroke(3, Color.parseColor("#1b496c"));
    	drawable.setCornerRadius(8);
    	drawable.setColor(Color.parseColor("#1b496c"));    	
    	
    	LinearLayout border = new LinearLayout(this.context);
    	
    	border.setBackgroundDrawable(drawable);

    	border.setPadding(5, 5, 5, 5);
    	
        TextView feedTitleText = new TextView(this.context);
        feedTitleText.setLayoutParams(new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        ));
        feedTitleText.setTextColor(Color.parseColor("#f9f9f9"));
        feedTitleText.setText(parseElement.getTitle());
        feedTitleText.setTextSize(16);
        feedTitleText.setTypeface(null, Typeface.BOLD);
        feedTitleText.setGravity(Gravity.CENTER);

        TextView feedShortContentText = new TextView(this.context);
        LinearLayout.LayoutParams paramFeedShortContentText = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        
        //paramFeedShortContentText.weight = (float) 1.0;
        feedShortContentText.setLayoutParams(paramFeedShortContentText);
        feedShortContentText.setText(this.parseElement.getShortContent());
        feedShortContentText.setTextSize(13);
        //feedShortContentText.setMaxLines(2);
        
        border.setGravity(Gravity.CENTER);
        border.addView(feedTitleText);
        
        LinearLayout.LayoutParams paramFeedContent = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        paramFeedContent.setMargins(20, 0, 20, 20);
        
        this.content = new LinearLayout(this.context);
        this.content.setLayoutParams(paramFeedContent);
        this.content.setOrientation(LinearLayout.VERTICAL);
        this.content.addView(border);
        this.content.addView(feedShortContentText);
        this.content.setOnClickListener(new OnClickListener()
		{
			
			@Override
			public void onClick(View v)
			{				
				final Handler han = new Handler()
				{
					@Override
					public void handleMessage(Message msg)
					{
					    super.handleMessage(msg);
					    
					    if(msg.what == 0)
					    {
					    	ParseArcticsuDetailedFeed parse = (ParseArcticsuDetailedFeed) msg.obj;
					    	
					    	Intent intent = new Intent(FeedItemContent.this.context, DetailedFeedActivity.class);
							intent.putExtra("title", parse.getTitle());
							//intent.putExtra("imageUrl", parse.getUrlImage());
							intent.putExtra("content", parse.getContent());
							
							FeedItemContent.this.context.startActivity(intent);
					    }
					    
					}
				};
				
				new Thread(new Runnable()
				{
					
					@Override
					public void run()
					{
						ParseArcticsuDetailedFeed parse = new ParseArcticsuDetailedFeed(ConstantsArctisuru.URL_ARCTICSU_RU + FeedItemContent.this.parseElement.getLink());
						
						Message mes = new Message();
						mes.what = 0;
						mes.obj = parse;
						
						han.sendMessage(mes);
					}
				}).start();
				
				
			}
		});

    }
    
	@Override
    public void update()
    {
		
    }

	@Override
    public View getView()
    {
	    return this.content;
    }
	
}
