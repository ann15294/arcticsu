package ru.arcticsu.activities;


import java.util.ArrayList;

import ru.arcticsu.R;
import ru.arcticsu.activities.gui.FeedItemContent;
import ru.arcticsu.tools.sites.arcticsuru.ParseArcticsuFeeds;
import ru.arcticsu.tools.sites.arcticsuru.elements.ParseElementShortFeed;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * @author Danilov
 *
 */
public class FeedsAcrivity extends RunnableActivity
{
	private Handler hen;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
		/**
		 *  We will not use setContentView in this activty 
		 *  Rather than we will use layout inflater to add view in FrameLayout of our base activity layout*/
		
		/**
		 * Adding our layout to parent class frame layout.
		 */
		getLayoutInflater().inflate(R.layout.runnable_activity, frameLayout);
		
		/**
		 * Setting title and itemChecked  
		 */
		mDrawerList.setItemChecked(position, true);
		setTitle(listArray[position]);
		
		//setContentView(R.layout.activity_main);
		
		final ArrayList<FeedItemContent> feedList = new ArrayList<FeedItemContent>();
		final LinearLayout linearLayout1 = (LinearLayout) findViewById(R.id.linearLayoutContainer);
		
		this.hen = new Handler()
		{
			public void handleMessage(Message msg)
			{
			    super.handleMessage(msg);
			    if(msg.what==0)
			    {
			    	FeedItemContent feed = (FeedItemContent)msg.obj;
			    
			    	linearLayout1.addView(feed.getView());
			    }
			}
		};
		
        Thread tread = new Thread(new Runnable() 
        {
            public void run() 
            {            	
            	ParseArcticsuFeeds arcticsu = new ParseArcticsuFeeds();
            	if(arcticsu.getArrayFeed()!=null)
            	{
		        	for(ParseElementShortFeed feed : arcticsu.getArrayFeed())
		        	{
		        		FeedItemContent item = new FeedItemContent(linearLayout1.getContext(), feed);
		        		feedList.add(item);
		        	}
            	}
            	
                if(feedList.size()==0)
                	Toast.makeText(FeedsAcrivity.this, "Не удалось подключится к www.arcticsu.ru", Toast.LENGTH_LONG).show();
                else        
        	        for(FeedItemContent feed : feedList)
        	        {
        	        	Message mes = new Message();
        	        	mes.obj = feed;
        	        	mes.what = 0;
        	        	hen.sendMessage(mes);
        	        }
          
           }
       });

        tread.start();
        

	}
}
